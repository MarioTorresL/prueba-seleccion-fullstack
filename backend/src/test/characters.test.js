const server = require ('../index');
const supertest = require('supertest');
const request = supertest(server)

describe("Characters",  ()=> {

  describe('GET', ()=>{

    it('Get all characters', async ()=> {
      // Sends GET Request to /test endpoint
      const res = await request.get('/characters')
      expect(res.status).toBe(200)
    })

    it('GET characters by id', async()=>{
      // Sends GET Request to /test endpoint
      const res = await request.get('/characters/3')
      expect(res.status).toBe(200)
    })

    it('GET characters by id but this id is not register', async()=>{
      
      const res = await request.get('/characters/'+ 999999)
      expect(res.status).toBe(404)
    })

  })

})