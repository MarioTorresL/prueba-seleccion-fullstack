'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    return queryInterface.createTable('Characters',{
      id:{
        type: Sequelize.INTEGER,
        allownull:false,
        primaryKey: true
      },
      url: {
        type: Sequelize.STRING,
        allownull:true
      },
      name:{
        type: Sequelize.STRING,
        allownull:true
      },
      gender:{
        type: Sequelize.STRING,
        allownull:true
      },
      culture:{
        type: Sequelize.STRING,
        allownull:true
      },
      born:{
        type: Sequelize.STRING,
        allownull:true
      },
      died:{
        type: Sequelize.STRING,
        allownull:true
      },
      titles:{
        type: Sequelize.ARRAY(Sequelize.STRING),
        allownull:true
      },
      aliases:{
        type: Sequelize.ARRAY(Sequelize.STRING),
        allownull:true
      },
      father:{
        type: Sequelize.STRING,
        allownull:true
      },
      mother:{
        type: Sequelize.STRING,
        allownull:true
      },
      spouse:{
        type: Sequelize.STRING,
        allownull:true
      },
      allegiances:{
        type: Sequelize.ARRAY(Sequelize.STRING),
        allownull:true
      },
      books:{
        type: Sequelize.ARRAY(Sequelize.STRING),
        allownull:true
      },
      povBooks:{
        type: Sequelize.ARRAY(Sequelize.STRING),
        allownull:true
      },
      tvSeries:{
        type: Sequelize.ARRAY(Sequelize.STRING),
        allownull:true
      },
      playedBy:{
        type: Sequelize.ARRAY(Sequelize.STRING),
        allownull:true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    return queryInterface.dropTable('Characters')
  }
};
