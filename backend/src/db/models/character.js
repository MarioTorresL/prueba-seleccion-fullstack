module.exports = (sequelize, DataTypes) => {
  const Character = sequelize.define('Character', {
    id:{
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    url: {
      type: DataTypes.STRING,
      allownull:true
    },
    name:{
      type: DataTypes.STRING,
      allownull:true
    },
    gender:{
      type: DataTypes.STRING,
      allownull:true
    },
    culture:{
      type: DataTypes.STRING,
      allownull:true
    },
    born:{
      type: DataTypes.STRING,
      allownull:true
    },
    died:{
      type: DataTypes.STRING,
      allownull:true
    },
    titles:{
      type: DataTypes.ARRAY(DataTypes.STRING),
      allownull:true
    },
    aliases:{
      type: DataTypes.ARRAY(DataTypes.STRING),
      allownull:true
    },
    father:{
      type: DataTypes.STRING,
      allownull:true
    },
    mother:{
      type: DataTypes.STRING,
      allownull:true
    },
    spouse:{
      type: DataTypes.STRING,
      allownull:true
    },
    allegiances:{
      type: DataTypes.ARRAY(DataTypes.STRING),
      allownull:true
    },
    books:{
      type: DataTypes.ARRAY(DataTypes.STRING),
      allownull:true
    },
    povBooks:{
      type: DataTypes.ARRAY(DataTypes.STRING),
      allownull:true
    },
    tvSeries:{
      type: DataTypes.ARRAY(DataTypes.STRING),
      allownull:true
    },
    playedBy:{
      type: DataTypes.ARRAY(DataTypes.STRING),
      allownull:true
    }
  }, {
      paranoid: true
  });

  return Character;
};