const axios = require('axios');
const { filter } = require('lodash');
const models = require('./db/models');

module.exports.getAllCharacters = async (ids, queries)=>{
  //check if it brings "querys"
  if(ids || queries){
    //*************create query******************
    var finalQuery='?';
    if(queries.page){
      finalQuery= finalQuery+ 'page=' + queries.page + '&'
    }
    if(queries.page){
      finalQuery=finalQuery+'pageSize='+queries.pageSize +'&'
    }
    if(queries.name){
      finalQuery=finalQuery+'name='+queries.name +'&'
    }
    if(queries.gender){
      finalQuery=finalQuery+'gender='+queries.gender +'&'
    }
    if(queries.culture){
      finalQuery=finalQuery+'culture='+queries.culture +'&'
    }
    if(queries.born){
      finalQuery=finalQuery+'born='+queries.born +'&'
    }
    if(queries.died){
      finalQuery=finalQuery+'died='+queries.died +'&'
    }
    if(queries.isAlive){
      finalQuery=finalQuery+'isAlive='+queries.isAlive +'&'
    }
    // *******************************************************

    //get with query
    const data = await axios.get('https://www.anapioficeandfire.com/api/characters'+finalQuery)
      .then(response => {
        //iteration of all characters
        response.data.forEach(data=>{
          // id of actual character
          characterId=data.url.split('/',6)
          // ************************

          //Check if it is entered in the db
          filterIds = ids.filter(id=> id !== characterId[5])
          // *****************************
          if(filterIds){
            //if exist exit
          }else{
            //if not exist create new character in db preserving oficial id
            data={
              id:characterId[5],
              url:data.url,
              name:data.name,
              gender:data.gender,
              culture:data.culture,
              born:data.born,
              died:data.died,
              titles:data.titles,
              aliases:data.aliases,
              father:data.father,
              mother:data.mother,
              spouse:data.spouse,
              allegiances:data.allegiances,
              books:data.books,
              povBooks:data.povBooks,
              tvSeries:data.tvSeries,
              playedBy:data.playedBy
            }
            models.Character.create(data)
          }
        })
      })
      .catch(error => {
        return error
      });


  }else{
    //setting first get without data in db with 10 first data
    const data = await axios.get('https://www.anapioficeandfire.com/api/characters?page=1&pageSize=10')
      .then(response => {
        //iteration of all characters and save in local db
        response.data.forEach(data=>{
          id = data.url.split('/',6)
          data={
            id:id[5],
            url:data.url,
            name:data.name,
            gender:data.gender,
            culture:data.culture,
            born:data.born,
            died:data.died,
            titles:data.titles,
            aliases:data.aliases,
            father:data.father,
            mother:data.mother,
            spouse:data.spouse,
            allegiances:data.allegiances,
            books:data.books,
            povBooks:data.povBooks,
            tvSeries:data.tvSeries,
            playedBy:data.playedBy
          }
          models.Character.create(data)
        })
      })
      .catch(error => {
        return error
      });
      return data
  }
}

module.exports.getOneCharacters = async (id)=>{
  //check if it brings id
  if(id){
    //get whit id
    const data = await axios.get('https://www.anapioficeandfire.com/api/characters/'+id)
      .then(response => {
          id = response.data.url.split('/',6)
          console.log('id', id)
          dataCharacter={
            id:id[5],
            url:response.data.url,
            name:response.data.name,
            gender:response.data.gender,
            culture:response.data.culture,
            born:response.data.born,
            died:response.data.died,
            titles:response.data.titles,
            aliases:response.data.aliases,
            father:response.data.father,
            mother:response.data.mother,
            spouse:response.data.spouse,
            allegiances:response.data.allegiances,
            books:response.data.books,
            povBooks:response.data.povBooks,
            tvSeries:response.data.tvSeries,
            playedBy:response.data.playedBy
          }
           models.Character.create(dataCharacter)
      })
      .catch(error => {
        return error
      });
      
      return data
  }
}