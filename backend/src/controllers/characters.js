const _ = require ('lodash');
const router = require('express').Router();
const {NotFound} = require('../api/errors');
const models = require('../db/models');
const utils = require('../utils');

router.get('/', async(req, res) =>{
  try{
    var array = [];
    const records = await models.Character.findAll()
    const query = req.query

    if(Object.entries(records).length==0){
      //update db with first 10 data
      utils.getAllCharacters()
    }else{

      records.forEach(character => {
        array.push(character.id)
      });
      utils.getAllCharacters(array,query)
      //find in local db with querys
      const pageSize =query.pageSize;
      const page = query.page;
      delete query.pageSize
      delete query.page
      const characters = await models.Character.findAll({
        where:query,
        limit: pageSize,
        offset: page
      })
      res.status(200).json(characters)
    }

  }catch(e){
    res.status(400).error(e)
  }
})

router.get('/:id', async(req, res) =>{
  try{
    const record = await models.Character.findByPk(req.params.id)
    if(record){
      res.status(200).json(record)
    }else{
      const record = await utils.getOneCharacters(req.params.id)
      if(record.message){
        res.status(404).error(new NotFound('Not found'))
      }
      if(!record.message){
        res.status(200).json(record)
      }
    }
  }catch(e){
    res.status(400).error(e)
  }
})

module.exports = router;